import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

int slow = 2

WebUI.scrollToPosition(0, 0)

WebUI.click(findTestObject('Object Repository/Aplikasi-Survey/a_Objek Pembiayaan'))

WebUI.click(findTestObject('Object Repository/Aplikasi-Survey/button_Tambah'))

WebUI.delay(slow)

WebUI.selectOptionByValue(findTestObject('Object Repository/Aplikasi-Survey/select_--SILAHKAN PILIH--003 - Mobil Baru'), 
    findTestData('Objek Pembiayaan').getValue(1, 1), true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Aplikasi-Survey/select_--SILAHKAN PILIH--00G - DAIHATSU'), findTestData(
        'Objek Pembiayaan').getValue(2, 1), true)

WebUI.click(findTestObject('Object Repository/Aplikasi-Survey/span_Model Objek_fa fa-search'))

WebUI.setText(findTestObject('Object Repository/Aplikasi-Survey/input_Search_form-control input-sm'), findTestData('Objek Pembiayaan').getValue(
        3, 1))

WebUI.click(findTestObject('Aplikasi-Survey/tabel_model_objek'))

WebUI.click(findTestObject('Object Repository/Aplikasi-Survey/button_Pilih'))

WebUI.setText(findTestObject('Object Repository/Aplikasi-Survey/input_Tahun Perakitan_inp-obj-pembiayaan-ta_790861'), findTestData(
        'Objek Pembiayaan').getValue(4, 1))

WebUI.setText(findTestObject('Object Repository/Aplikasi-Survey/input_Harga Objek_inp-obj-pembiayaan-harga-objek'), findTestData(
        'Objek Pembiayaan').getValue(5, 1))

if (findTestData('Aplikasi').getValue(6, 1) != '01 - DEALER/SHOWROOM') {
    WebUI.selectOptionByLabel(findTestObject('Aplikasi/select_--SILAHKAN PILIH--01 - DEALERSHOWROO_7d5858 (1)'), findTestData(
            'Aplikasi').getValue(6, 1), true)

    WebUI.click(findTestObject('Aplikasi/klik_search_objek_pembiayaan'))

    WebUI.setText(findTestObject('Aplikasi/search_outlet_objek_pembiayaan'), findTestData('Objek Pembiayaan').getValue(32, 
            1))

    WebUI.click(findTestObject('Aplikasi/tabel_outlet_objek_pembiayaan'))

    WebUI.click(findTestObject('Object Repository/Dokumen/button_Pilih'))
}

WebUI.selectOptionByLabel(findTestObject('Object Repository/Aplikasi-Survey/select_--SILAHKAN PILIH--92000001-REKENING DEALER'), 
    findTestData('Objek Pembiayaan').getValue(8, 1), true)

WebUI.selectOptionByLabel(findTestObject('Object Repository/Aplikasi-Survey/select_--SILAHKAN PILIH--05-PRIBADI06-OPERA_e857a6'), 
    findTestData('Objek Pembiayaan').getValue(6, 1), true)

WebUI.setText(findTestObject('Object Repository/Aplikasi-Survey/input_Atas Nama BPKB _inp-nama-bpkb'), findTestData('Objek Pembiayaan').getValue(
        7, 1))

WebUI.click(findTestObject('Object Repository/Aplikasi-Survey/input_No Invoice _btn-save-objek-pembiayaan_2d5fc7'))

WebUI.delay(slow)

WebUI.click(findTestObject('Object Repository/Aplikasi-Survey/button_Ya'))

WebUI.click(findTestObject('Object Repository/Aplikasi-Survey/button_OK'))

WebUI.scrollToPosition(0, 0)

WebUI.click(findTestObject('Object Repository/Aplikasi-Survey/a_Struktur Kredit'))

WebUI.selectOptionByLabel(findTestObject('Object Repository/Aplikasi-Survey/select_--SILAHKAN PILIH--23 - BURSA MOBIL01_45a208'), 
    findTestData('Objek Pembiayaan').getValue(9, 1), true)

WebUI.selectOptionByLabel(findTestObject('Object Repository/Aplikasi-Survey/select_--SILAHKAN PILIH--BASIC-PB-BRNCR0203_dec01e'), 
    findTestData('Objek Pembiayaan').getValue(10, 1), true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Aplikasi-Survey/select_--SILAHKAN PILIH--01 - ANNUITY02 - D_30728d'), 
    findTestData('Objek Pembiayaan').getValue(11, 1), true)

WebUI.setText(findTestObject('Object Repository/Aplikasi-Survey/input_Tenor_inp-top'), findTestData('Objek Pembiayaan').getValue(
        12, 1))

WebUI.selectOptionByLabel(findTestObject('Object Repository/Aplikasi-Survey/select_--SILAHKAN PILIH--01 - IN ADVANCE02 _ab4be3'), 
    findTestData('Objek Pembiayaan').getValue(13, 1), true)

if(findTestData("Objek Pembiayaan").getValue(1, 1)=='001' || findTestData("Objek Pembiayaan").getValue(1, 1)=='002')
{
	WebUI.setText(findTestObject('Object Repository/Aplikasi-Survey/input_Uang Muka (Net)_inp-gross-dp'), findTestData('Objek Pembiayaan').getValue(
		33, 1))
}
else
{
	WebUI.setText(findTestObject('Object Repository/Aplikasi-Survey/input_Uang Muka (Net)_inp-net-dp'), findTestData('Objek Pembiayaan').getValue(
        14, 1))
}

WebUI.setText(findTestObject('Object Repository/Aplikasi-Survey/input_Uang Muka (Cabang)_inp-dp-branch'), findTestData('Objek Pembiayaan').getValue(
        15, 1))

WebUI.click(findTestObject('Object Repository/Aplikasi-Survey/input_Rate_cal-rate'))

WebUI.setText(findTestObject('Object Repository/Aplikasi-Survey/input_Suku Bunga (Eff)_inp-bunga-eff'), findTestData('Objek Pembiayaan').getValue(
        17, 1))

WebUI.setText(findTestObject('Object Repository/Aplikasi-Survey/input_Biaya Kredit_inp-biaya-kredit-provisi'), findTestData(
        'Objek Pembiayaan').getValue(19, 1))

WebUI.delay(slow)

WebUI.setText(findTestObject('Object Repository/Aplikasi-Survey/input_Total Biaya_inp-biaya-total-provisi'), findTestData(
        'Objek Pembiayaan').getValue(20, 1))

WebUI.setText(findTestObject('Object Repository/Aplikasi-Survey/input_Biaya Kredit_inp-biaya-kredit-admin'), findTestData(
        'Objek Pembiayaan').getValue(21, 1))

WebUI.delay(slow)

WebUI.setText(findTestObject('Object Repository/Aplikasi-Survey/input_Total Biaya_inp-biaya-total-admin'), findTestData(
        'Objek Pembiayaan').getValue(22, 1))

WebUI.selectOptionByValue(findTestObject('Object Repository/Aplikasi-Survey/select_--SILAHKAN PILIH--02 - MANDIRI INSUR_a5bfda'), 
    findTestData('Objek Pembiayaan').getValue(23, 1), true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Aplikasi-Survey/select_--SILAHKAN PILIH--0 - NOT INSR1 - TO_d47ba7'), 
    findTestData('Objek Pembiayaan').getValue(24, 1), true)

WebUI.setText(findTestObject('Object Repository/Aplikasi-Survey/input_Biaya Kredit Asuransi_inp-biaya-kredi_85ac16'), findTestData(
        'Objek Pembiayaan').getValue(25, 1))

WebUI.delay(slow)

WebUI.setText(findTestObject('Object Repository/Aplikasi-Survey/input_Biaya Total Asuransi_inp-biaya-total-_f057e6'), findTestData(
        'Objek Pembiayaan').getValue(26, 1))

if (findTestData('Objek Pembiayaan').getValue(28, 1) != '') {
    WebUI.selectOptionByLabel(findTestObject('Object Repository/Aplikasi-Survey/select_--SILAHKAN PILIH--PERSONAL ACCIDENT _3faa0f'), 
        findTestData('Objek Pembiayaan').getValue(28, 1), true)

    WebUI.selectOptionByValue(findTestObject('Object Repository/Aplikasi-Survey/select_--SILAHKAN PILIH--03 - ASWATA INSURA_371328'), 
        findTestData('Objek Pembiayaan').getValue(29, 1), true)

    WebUI.setText(findTestObject('Object Repository/Aplikasi-Survey/input_Biaya Tunai Asuransi_inp-biaya-tunai-_4cdbcf'), 
        findTestData('Objek Pembiayaan').getValue(30, 1))

    WebUI.delay(slow)

    WebUI.setText(findTestObject('Object Repository/Aplikasi-Survey/input_Biaya Kredit Asuransi_inp-biaya-kredi_bd3e1c'), 
        findTestData('Objek Pembiayaan').getValue(31, 1))
}

WebUI.click(findTestObject('Object Repository/Aplikasi-Survey/input_Angsuran Terakhir_btn-save-rincian-pinjaman'))

WebUI.click(findTestObject('Object Repository/Aplikasi-Survey/input_Angsuran Terakhir_btn-save-rincian-pinjaman'))

WebUI.click(findTestObject('Object Repository/Aplikasi-Survey/button_Ya'))

WebUI.delay(slow)

WebUI.click(findTestObject('Object Repository/Aplikasi-Survey/button_OK'))

WebUI.scrollToPosition(0, 0)

WebUI.click(findTestObject('Object Repository/Aplikasi-Survey/a_Refund'))

WebUI.click(findTestObject('Object Repository/Aplikasi-Survey/input_Penerima Komisi Langsung Perantara_bt_43ab13'))

WebUI.click(findTestObject('Object Repository/Aplikasi-Survey/button_Ya'))

WebUI.click(findTestObject('Object Repository/Aplikasi-Survey/button_OK'))

