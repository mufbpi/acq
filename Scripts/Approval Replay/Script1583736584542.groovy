import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

int slow = 5

WebUI.click(findTestObject('credit_approval/i_Tidak_fa fa-pencil'))

WebUI.delay(slow)

WebUI.click(findTestObject('credit_approval/a_Aplikasi'))

WebUI.delay(slow)

WebUI.click(findTestObject('credit_approval/a_Survey'))

WebUI.delay(slow)

WebUI.click(findTestObject('credit_approval/a_Dokumen'))

WebUI.delay(slow)

WebUI.click(findTestObject('credit_approval/a_Dokumen PKAP'))

WebUI.delay(slow)

WebUI.click(findTestObject('credit_approval/a_Approval'))

WebUI.delay(slow)

WebUI.delay(slow)

WebUI.click(findTestObject('credit_approval/button_OK'))

WebUI.delay(slow)

WebUI.delay(slow)

WebUI.selectOptionByLabel(findTestObject('credit_approval/select_--SILAHKAN PILIH--APPD-Approve  Docu_f606d3'), findTestData(
        'Credit Approval').getValue(3, 3), true)

WebUI.setText(findTestObject('credit_approval/textarea_Approval Note_txt-approval-note'), findTestData('Credit Approval').getValue(
        3, 4))

WebUI.selectOptionByLabel(findTestObject('credit_approval/select_--SILAHKAN PILIH--02 - NON SCORE'), findTestData('Credit Approval').getValue(
        2, 5), true)

WebUI.selectOptionByLabel(findTestObject('credit_approval/select_--SILAHKAN PILIH--11-ANALISA REKENIN_3ba677'), findTestData(
        'Credit Approval').getValue(2, 6), true)

WebUI.selectOptionByLabel(findTestObject('credit_approval/select_--SILAHKAN PILIH--00000004-APPROVED _ee833c'), findTestData(
        'Credit Approval').getValue(2, 7), true)

WebUI.delay(slow)

WebUI.click(findTestObject('credit_approval/button_Save'))

WebUI.click(findTestObject('credit_approval/button_OK'))

WebUI.delay(slow)

WebUI.scrollToPosition(0, 0)

WebUI.click(findTestObject('credit_approval/a_NURSANTI KUSUMAASTUTI AFFANDI'))

WebUI.click(findTestObject('credit_approval/a_Log Out'))

