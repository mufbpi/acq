import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl(findTestData('Login').getValue(1, 1))

WebUI.setText(findTestObject('Login/input_Silahkan Login_id-login'), findTestData('Login').getValue(2, 1))

WebUI.setText(findTestObject('Login/input_Silahkan Login_pass-login'), findTestData('Login').getValue(3, 1))

WebUI.click(findTestObject('Object Repository/Purchase Order/button_Login'))

WebUI.maximizeWindow()

WebUI.click(findTestObject('Object Repository/Purchase Order/a_PURCHASE ORDER'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Purchase Order/a_GENERATED PO'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Purchase Order/span_Dealer_fa fa-search'))

WebUI.delay(1)

WebUI.setText(findTestObject('Object Repository/Purchase Order/input_Search_form-control input-sm'), findTestData('Purchase Order').getValue(
        1, 1))

WebUI.click(findTestObject('Object Repository/Purchase Order/td_PT CITRAKARYA PRANATA'))

WebUI.click(findTestObject('Object Repository/Purchase Order/button_Pilih'))

WebUI.selectOptionByLabel(findTestObject('Object Repository/Purchase Order/select_--SILAHKAN PILIH--1700002074 - ROBER_4391d4'), 
    findTestData('Purchase Order').getValue(2, 1), true)

WebUI.click(findTestObject('Object Repository/Purchase Order/button_Search'))

WebUI.setText(findTestObject('Purchase Order/search_po_no_aplikasi'), findTestData('Import').getValue(2, 1))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Purchase Order/input_Amount_check-po-0'))

WebUI.selectOptionByLabel(findTestObject('Object Repository/Purchase Order/select_--SILAHKAN PILIH--REA - READY STOCKI_9c3c27'), 
    findTestData('Purchase Order').getValue(4, 1), true)

WebUI.click(findTestObject('Object Repository/Purchase Order/button_Print PDF'))

WebUI.click(findTestObject('Object Repository/Purchase Order/button_Ya'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Purchase Order/button_OK'))

WebUI.delay(3)

WebUI.closeBrowser()

