<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>form_Jenis Identitas--SILAHKAN PILIH--01 - _d5a875</name>
   <tag></tag>
   <elementGuidId>907927ac-6d31-4416-a43f-dec451b26539</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;form-pic-management&quot;)[count(. | //*[@id = 'form-pic-management']) = count(//*[@id = 'form-pic-management'])]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='form-pic-management']
//*[@id=&quot;slc-jenis-identitas&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>form</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>form-pic-management</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>panel-body</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
			
				
					
						
							
								Jenis Identitas
								:
							
							
								--SILAHKAN PILIH--01 - KTP03 - PASSPORT06 - RESI KTP
							
						
						
							
								No Identitas:
							
							
								
							
						
						
                                
                                    Tanggal Berlaku Identitas:
                                
                                
                                    
                                        
                                            
                                                
                                                
                                                    
                                                
                                            
                                            
                                        
                                        s/d
                                        
                                            
                                                
                                                
                                                    
                                                
                                            
                                            
                                        
                                    
                                
                            
						
							
								Nama Lengkap Seusai Identitas:
							
							
								
							
						
						
							
								Nama Lengkap
								:
							
							
								
									
								
							
						
						
                            
                                Agama 
                                :
                            
                            
                                --SILAHKAN PILIH--01 - ISLAM02 - KRISTEN03 - KATOLIK04 - HINDU05 - BUDHA99 - -06 - KONGHUCU07 - LAINNYA
                            
                        
                        
	                        
	                            Status Pernikahan:
	                        
	                        
	                            --SILAHKAN PILIH--01 - KAWIN02 - SINGLE03 - DUDA/JANDA TANPA ANAK04 - DUDA/JANDA DGN ANAK
	                        
	                    
						
							
								Gelar:
							
							
								
								
							
							
						
					
					
						
							
								
									Tanggal Lahir :
								
								
									
									    
									    
									        
									    
									
									
								
							
							
								
									Tempat Lahir Sesuai Identitas :
								
								
									
								
							
							
                                
                                    Jenis Kelamin:
                                
                                
                                    --SILAHKAN PILIH--L - LAKI LAKIP - PEREMPUAN
                                
                            
                            
                            
								
									Pemilik
									:
								
								
									
										
											--SILAHKAN PILIH--
											YA
                                        	TIDAK
										
									
								
							
							
							
								
									Jabatan
									:
								
								
									
										
											--SILAHKAN PILIH--
										
									
								
							
							
								
									No Handphone:
								
								
									
								
							
							
								
									Email:
								
								
									
								
							
							
							
								
									Status Pengurus/Pemilik :
									
								
								
									
										AKTIF
										TELAH BERAKHIR
									
								
							
							
							
							  
							  
							
						 	        
					
				
			
			
				Informasi Alamat
				
			
			
				  
					AlamatRTRWKelurahanKecamatanKab/KotaProvinsiKode PosTeleponFaxAksi 
				  
				  
					
				  No data available in table
			
			
		</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;form-pic-management&quot;)</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//form[@id='form-pic-management']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='block-pic-main']/form</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Customer Perusahaan – PIC Manajemen'])[1]/following::form[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Perbandingan Pendapatan (DIR) %'])[2]/following::form[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/form</value>
   </webElementXpaths>
</WebElementEntity>
