<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_--SILAHKAN PILIH--001 - AccountingFi_20d0c1</name>
   <tag></tag>
   <elementGuidId>0a3bb244-8ae1-48bc-ad64-8d754fc21a10</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='pilih-pekerjaan']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>pilih-pekerjaan</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control corr-disabled</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tablename</name>
      <type>Main</type>
      <value>MUFACQ.APPLICANT_PERSONAL</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>fieldname</name>
      <type>Main</type>
      <value>OCCUPATION_CODE</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>fieldlabel</name>
      <type>Main</type>
      <value>Pekerjaan - Pekerjaan</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                    --SILAHKAN PILIH--001 - Accounting/Finance Officer002 - Customer service 003 - Engineering 004 - Eksekutif 005 - Administrasi umum 006 - Teknologi Informasi 007 - Konsultan/Analis 008 - Marketing 009 - Pengajar (Guru, Dosen)010 - Militer011 - Pensiunan 012 - Pelajar/Mahasiswa 013 - Wiraswasta 014 - Polisi 015 - Petani 016 - Nelayan 017 - Peternak 018 - Dokter 019 - Tenaga Medis (Perawat, Bidan, dsb)020 - Hukum (Pengacara, Notaris)021 - Perhotelan &amp; Restoran (Koki, Bartender, dsb)022 - Peneliti 023 - Desainer 024 - Arsitek 025 - Pekerja Seni (Artis, Musisi, Pelukis, dsb)026 - Pengamanan 027 - Pialang/Broker 028 - Distributor 029 - Transportasi Udara (Pilot, Pramugari)030 - Transportasi Laut (Nahkoda, ABK)031 - Transportasi Darat (Masinis, Sopir, Kondektur)032 - Buruh (Buruh Pabrik, Buruh Bangunan, Buruh Tani)033 - Pertukangan &amp; Pengrajin (Tukang Kayu, Pengrajin Kulit, dll)034 - Ibu Rumah Tangga 035 - Pekerja Informal (Asisten Rumah Tangga, Asongan, dll)036 - Pejabat Negara/Penyelenggara Negara 037 - Pegawai Pemerintahan/Lembaga Negara (selain Pejabat/Penyelenggara Negara) 099 - Lain-lain 038 - Politically Exposed Person*039 - Wartawan</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;pilih-pekerjaan&quot;)</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='pilih-pekerjaan']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='pekerjaan_perorangan']/div/div[2]/div/div/div/div/div[2]/div/select</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pekerjaan'])[5]/following::select[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pekerjaan Sekarang'])[1]/preceding::select[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[2]/div/div/div/div/div[2]/div/select</value>
   </webElementXpaths>
</WebElementEntity>
