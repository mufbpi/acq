<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_--SILAHKAN PILIH--10 - Kredit Modal _660fd4</name>
   <tag></tag>
   <elementGuidId>0aed0c9e-c77d-4268-ac6d-1efe573206d1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='slc-aplikasi-tipe-kredit']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>slc-aplikasi-tipe-kredit</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control corr-disabled</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-live-search</name>
      <type>Main</type>
      <value>true</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>--SILAHKAN PILIH--10 - Kredit Modal Kerja Permanen (KMKP)16 - Kredit Umum Pedesaan (Kupedes)18 - Kredit kelolaan25 - Kredit Perkebunan Swasta Nasional (PSN)26 - Kredit Ekspor28 - Modal Kerja - Kredit Koperasi - Kredit Usaha Tani (KUT)32 - Modal Kerja - Kredit Koperasi - Kredit kepada Koperasi Unit Desa (KUD)36 - Modal Kerja - Kredit Koperasi - Kredit kepada Koperasi Primer untuk Anggotanya38 - Modal Kerja - Kredit Koperasi - Lainnya39 - Kredit modal kerja lainnya42 - Investasi - Kredit Investasi Kecil (KIK)45 - Investasi - PIR-BUN - Kredit Kebun Inti46 - Investasi - PIR-BUN - Kredit Kebun Plasma47 - Investasi - PIR-BUN - Kredit Pasca Konversi PIR-BUN48 - Investasi - UPP - Kredit Peremajaan Rehabilitasi Perluasan Tanaman Ekspor (PRPTE)49 - Investasi - UPP - Kredit Pasca Konversi PRPTE50 - Investasi - UPP - Lainnya51 - Investasi - PIR-TRANS - Kredit Kebun Inti52 - Investasi - PIR-TRANS - Kredit Kebun Plasma53 - Investasi - PIR-TRANS - Kredit Pasca Konversi54 - Investasi - Kredit Perkebunan Swasta Nasional (PSN)55 - Investasi - Bantuan Proyek - Nilai lawan valuta asing56 - Investasi - Bantuan Proyek - Biaya lokal Rekening Dana Investasi (RDI)57 - Investasi - Bantuan Proyek - Biaya lokal dana perbankan59 - Investasi - Kredit kelolaan di luar bantuan proyek60 - Investasi - Kredit Umum Pedesaan (Kupedes)62 - Investasi - Kredit Koperasi - Kredit kepada Koperasi Primer untuk Anggotanya63 - Investasi - Kredit Koperasi - Lainnya64 - Investasi - DLBS - Nilai lawan valuta asing67 - Investasi - DLBS - Kredit Rupiah74 - Investasi - Kredit Investasi sampai dengan Rp. 75 juta75 - Investasi - Kredit Investasi Biasa76 - Investasi - Kredit Ekspor79 - Investasi - Kredit Investasi Lainnya80 - KPR Sangat Sederhana (KPRSS) dan Kredit Pemilikan Kapling Siap Bangun (PKSB)81 - Pemilikan Rumah KPR Sederhana (KPRS) s.d. Tipe 2182 - Pemilikan Rumah Di atas tipe 21 s.d tipe 7083 - Pemilikan Rumah Di atas tipe 7085 - Perbaikan/Pemugaran Rumah86 - Kredit Kepada Guru untuk Pembelian Sepeda Motor (KPG)87 - Kredit Mahasiswa Indonesia88 - Kredit Rumah Toko89 - Kredit Konsumsi Lainnya</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;slc-aplikasi-tipe-kredit&quot;)</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='slc-aplikasi-tipe-kredit']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='slc-de-app-tipe-kredit']/select</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tipe Kredit:'])[1]/following::select[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Group Customer:'])[1]/following::select[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Golongan Kredit:'])[1]/preceding::select[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bank Pendanaan:'])[1]/preceding::select[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[2]/div/div[2]/div/div/div[2]/div/select</value>
   </webElementXpaths>
</WebElementEntity>
