<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>CA-FDE</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>c271ea48-2c24-48ee-a7f8-e241f5e33ac4</testSuiteGuid>
   <testCaseLink>
      <guid>8d5b05dd-d78a-4d40-836a-bd5b9ead7e5c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Credit Approval</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e8f13ac8-9c51-495d-960b-e2a9153b39e8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Purchase Order</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3e41adee-b5cc-453c-a038-875ede3e2b4c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/FDE</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
